# DEMO guide

## Setup

1. Launch ansible-cm container

```
docker run -v ~/Documents/xpand-bigtalk-ansible-demo:/home --name ansible-cm -it ansible-cm bash
```

2. Create nodes

```bash
docker run -d --rm -p 22 -p 8080:8080 --name hn01 ansible-target
docker run -d --rm -p 22 -p 8081:8080 --name en01 ansible-target
docker run -d --rm -p 22 -p 8082:8080 --name en02 ansible-target
docker run -d --rm -p 22 -p 8091:8080 --name dn01 ansible-target
docker run -d --rm -p 22 -p 8092:8080 --name dn02 ansible-target
```

2. Get Nodes IPs

```bash
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' hn01
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' en01
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' en02
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dn01
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dn02
```

3. Add IPs to inventory file in /etc/ansible/hosts

```
[headnodes]
172.17.0.3

[edgenodes]
172.17.0.4
172.17.0.5

[datanodes]
172.17.0.6
172.17.0.7

```

4. SSH each node from ansible-cm to add ssh key to known_hosts. **Target machines password:** `centos`

```bash
ssh centos@172.17.0.3
ssh centos@172.17.0.4
ssh centos@172.17.0.5
ssh centos@172.17.0.6
ssh centos@172.17.0.7
```

## Orchestration 

### 1. Ping nodes

```bash
ansible all -m ping -u centos -k -b
ansible headnodes -m ping -u centos -k -b
ansible datanodes -m ping -u centos -k -b
```

### 2. Copy a file
```bash
ansible datanodes -m copy -a "src=/home/ansible/files/myscript.sh dest=/home/centos/" -u centos -k -b
```

### 3. Execute a command
```bash
ansible all -a "ls" -u centos -k -b
ansible datanodes -a "sh myscript.sh" -u centos -k -b
```

## Configuration - Playbooks

### Playbook 1
1. Execute a playbook
```bash
cd /home/ansible/
ansible-playbook playbooks/p1.yaml -u centos -k -b
```

2. verify changes
```
ansible all -a "git --version" -u centos -k -b
```
1. Re-run to see if there are new changes 
```bash
ansible-playbook playbooks/p1.yaml -u centos -k -b
```

### Playbook 2
1. Run playbook
```bash
cd /home/ansible/
ansible-playbook playbooks/p2.yaml -u centos -k -b
```
2. Verify
```bash
ansible all -a "ls" -u centos -k -b
```